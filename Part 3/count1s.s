	.text
	.global	_start
_start:
	movia r8, TEST_NUM
	movia r11, LENGTH_WORD 	/* store the address of size of word list */
	movia r27, 0x007ffffc  	/* initialize stack pointer to points at the bottom of memory */
	ldw r9, (r8) 			/* load content of word TEST_NUM into r9 register */
	ldw r13, (r11)			/* load word list size from address into r13 register */
	
STRING_COUNTER:	
	call ONES  				/* subroutine to calculate maximum number of 1s in a word */
	addi r8,r8,0x4			/* increment address by 4 to point to the next word in memory */
	ldw r9, (r8)			/* load word from address into r9 register, r9 will be used by subroutine to perform calculation */
	subi r13, r13, 0x1		/* decrement counter that counts the word list (i.e. from 10 to 0) */
	bne r13, r0, STRING_COUNTER 	/* if the counter is not 0, keep going over this loop */

END:
	br END					/* stay here if done */
	
ONES:	
	subi r27, r27, 12		/* substract stack pointer to point at higher address to store more registers content into stack memory */
	stw r11, 0(r27)
	stw r31, 4(r27)
	stw r9, 8(r27)
	
	mov r10, r0				/* reset r10 register to start counting from 0 */
	STRING_COUNTER_LOOP:
		beq r9, r0, END_STRING_COUNTER		/* if the content in r9 register is equal to zero, jump out of loop */
		srli r11, r9, 0x01					/* shift right by one, insert 0 from left */
		and r9, r9, r11						/* perform and operation to remove 1s from the word */
		addi r10, r10, 1					/* increment counter of largest 1 */
		br STRING_COUNTER_LOOP				/* keep looping */
	
	END_STRING_COUNTER:	
		bgeu r12, r10, END_ONES				/* compare r12 to r10, if r10 is smaller, don't care */
		mov r12, r10						/* if r10 is larger, store its value into r12 */
		ldw r9, 8(r27)						/* read unaltered r9 register value to be stored as the word with longest 1 */
		mov r2, r9							/* store word with longest 1 into r2 register */
	
	END_ONES:		
		ldw r11, 0(r27)						/* load from stack memory back to registers to restore original values before subroutine */
		ldw r31, 4(r27)
		ret

	.data
TEST_NUM:									/* words stored into memory */
	.word 0x000ff000
	.word 0x000ffff0
	.word 0xff000000
	.word 0x3fcd112f
	.word 0x3f13515f
	.word 0x3f1143e2
	.word 0x324244ef
	.word 0x3f11d0ef
	.word 0x124bed01
	.word 0x3fabe323
LENGTH_WORD:
	.word 0xa								/* represents 10 words are stored in the list above */
	.end