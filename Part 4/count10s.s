	.text
	.global	_start
_start:
	movia r8, TEST_NUM
	movia r11, LENGTH_WORD		/* store the address of size of word list */
	movia r27, 0x007ffffc 		/* initialize stack pointer to points at the bottom of memory */
	ldw r9, (r8)				/* load content of word TEST_NUM into r9 register */
	ldw r13, (r11)				/* load word list size from address into r13 register */
	
STRING_COUNTER:
	
	call ONES 									/* subroutine to calculate maximum number of 1s in a word */
		bgeu r12, r10, SKIP_UPDATE_ONES			/* compare r12 to r10, if r10 is smaller, don't care */
		mov r12, r10							/* if r10 is larger, store its value into r12 */
		mov r2, r9								/* if r10 is larger, update word with longest 1 into r2 */
	SKIP_UPDATE_ONES:
	call ZEROS									/* subroutine to calculate maximum number of 0s in a word */
		bgeu r14, r10, SKIP_UPDATE_ZEROS		/* compare r14 to r10, if r10 is smaller, don't care */
		mov r14, r10							/* if r10 is larger, store its value into r14 */
		mov r4, r9								/* if r10 is larger, update word with longest 0 into r4 */
	SKIP_UPDATE_ZEROS:
	call ALTERNATE								/* subroutine to calculate maximum number of alternating 1 and 0s in a word */
	
	addi r8,r8,0x4								/* increment address by 4 to point to the next word in memory */
	ldw r9, (r8)								/* load word from address into r9 register, r9 will be used by subroutine to perform calculation */
	subi r13, r13, 0x1							/* decrement counter that counts the word list (i.e. from 10 to 0) */
	bne r13, r0, STRING_COUNTER					/* if the counter is not 0, keep going over this loop */

END:	
	br END										/* stay here if done */
	
ONES:	
	subi r27, r27, 8							/* substract stack pointer to point at higher address to store more registers content into stack memory */
	stw r11, 0(r27)
	stw r9, 4(r27)
	
	mov r10, r0									/* reset r10 register to start counting from 0 */
	STRING_COUNTER_LOOP:
		beq r9, r0, END_STRING_COUNTER			/* if the content in r9 register is equal to zero, jump out of loop */
		srli r11, r9, 0x01						/* shift right by one, insert 0 from left */
		and r9, r9, r11							/* perform and operation to remove 1s from the word */
		addi r10, r10, 1						/* increment counter of largest 1 */
		br STRING_COUNTER_LOOP					/* keep looping */
	
	END_STRING_COUNTER:	
		ldw r11, 0(r27)							/* load from stack memory back to registers to restore original values before subroutine */
		ldw r9, 4(r27)
		addi r27, r27, 8
		ret
		
ZEROS:	
	subi r27, r27, 20							/* substract stack pointer to point at higher address to store more registers content into stack memory */
	stw r11, 0(r27)
	stw r9, 4(r27)
	stw r15, 8(r27)
	stw r18, 12(r27)
	stw r17, 16(r27)
	
	mov r10, r0									/* reset r10 register to start counting from 0 */
	movia r15, FULL_OF_ONE						/* let r15 register stores the address of FULL_OF_ONE */
	ldw r18, (r15)								/* load 0xffffffff into r18 from memory to be compared later */
	movia r15, FIRST_CHAR_ONE					/* let r15 register stores another address, FIRST_CHAR_ONE */
	ldw r17, (r15)								/* load 0x80000000 into r18 from memory to be used for masking shift right operation later */

	STRING_COUNT_ZERO_LOOP:
		beq r9, r18, END_STRING_COUNT_ZERO		/* if the content in r9 register is all 1, jump out of loop */
		srli r11, r9, 0x01						/* shift right by one, insert 0 from left (actually I want to insert 1 from left, but no operation for that */
		or r9, r9, r11							/* to fix the problem mentioned above, use 1 as the Most Significant Bit to make the first bit 1 */
		or r9, r9, r17							/* perform or operation to remove 0s from the word */
		addi r10, r10, 1						/* increment counter of largest 0 */
		br STRING_COUNT_ZERO_LOOP				/* keep looping */
	
	END_STRING_COUNT_ZERO:		
		ldw r11, 0(r27)							/* load from stack memory back to registers to restore original values before subroutine */
		ldw r9, 4(r27)
		ldw r15, 8(r27)
		ldw r18, 12(r27)
		ldw r17, 16(r27)
		addi r27, r27, 20
		ret

ALTERNATE:
	
	subi r27, r27, 28							/* substract stack pointer to point at higher address to store more registers content into stack memory */
	stw r11, 0(r27)
	stw r13, 4(r27)
	stw r17, 8(r27)
	stw r18, 12(r27)
	stw r19, 16(r27)
	stw r31, 20(r27)							/* since I'm going to call another subroutines, I need to store the return address to prevent conflicts of being overwritten by subroutine */
	stw r9, 24(r27)
	
	movia r11, FULL_OF_ALTERNATE				/* use r11 to store the address of alternating pattern in memory */
	ldw r13, (r11)								/* load content of alternating pattern into r13 */
	xor r9, r13, r9								/* use XOR operation to change alternating pattern into consecutive 1s or 0s */
	call ZEROS									/* call ZERO subroutine to find maximum 0 */
	mov r17, r10								/* move the content of r10 into r17 as r10 will be overwritten by the next subroutine */
	call ONES									/* call ONE subroutine to find maximum 1 */
	mov r18, r10								/* move the content of r10 into r18 */
	
	bgeu r17, r18, STORE_R17					/* compare r17 and r18. If r17 is larger, store r17 */
		mov r19, r18							/* if r18 is larger (the previous condition is false), store r18 into r19 */
		bgeu r19, r0, END_ALT_COMP				/* this instruction is ALWAYS TRUE, just to jump to END_ALT_COMP. Note: I tried JMP command but it doesn't work. Don't know why. */
	
	STORE_R17:
		mov r19, r17							/* this is jumped from bgeu at line 97 when r17 is larger. Store r17 into r19 and move on */
	
	
	END_ALT_COMP:
		bgeu r16, r19, END_ALTERNATES			/* compare with r16 register to check if this new maximum alternative is longer than previous records */
		mov r16, r19							/* if yes, store into r16 */
		ldw r9, 24(r27)							/* if yes, restore r9 value from memory stack to be stored as word with longest alternating 0 and 1 */
		mov r6, r9								/* load into register r6 */
		
	END_ALTERNATES:
		
		ldw r11, 0(r27)
		ldw r13, 4(r27)
		ldw r17, 8(r27)
		ldw r18, 12(r27)
		ldw r19, 16(r27)
		ldw r31, 20(r27)
	ret
	

	.data
TEST_NUM:
	.word 0x0000000a
	.word 0xaaaaaa00
	.word 0xff00000f
	.word 0xffffffff
	.word 0x3f13515f
	.word 0x3f1143e2
	.word 0x324244ef
	.word 0x3f11d0ef
	.word 0x124bed01
	.word 0x3fabe323
LENGTH_WORD:
	.word 0xa
FULL_OF_ONE:
	.word 0xffffffff
FIRST_CHAR_ONE:
	.word 0x80000000
FULL_OF_ALTERNATE:
	.word 0xaaaaaaaa
	.end