	.text
	.global	_start
_start:
	movia r8, TEST_NUM
	ldw r9, (r8)
	
STRING_COUNTER:
	mov r10, r0
STRING_COUNTER_LOOP:
	beq r9, r0, END_STRING_COUNTER
	srli r11, r9, 0x01
	and r9, r9, r11
	addi r10, r10, 0x01
	br STRING_COUNTER_LOOP
END_STRING_COUNTER:
	mov r12, r10

END:
	br END

	.data
TEST_NUM:
	.word 0x3fabedef
	.end